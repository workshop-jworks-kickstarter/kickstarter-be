package be.ordina.jworks.kickstarterbe.login.service;


import be.ordina.jworks.kickstarterbe.login.model.ModalData;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LoginService {

    public ModalData login(String username) {
        ModalData data = new ModalData();
        data.setUsername(username);
        data.setInfo(UUID.randomUUID().toString());
        return data;
    }

}
