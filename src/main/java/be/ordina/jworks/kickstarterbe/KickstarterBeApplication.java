package be.ordina.jworks.kickstarterbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KickstarterBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KickstarterBeApplication.class, args);
	}

}
