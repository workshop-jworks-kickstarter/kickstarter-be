package be.ordina.jworks.kickstarterbe.login.controller;

import be.ordina.jworks.kickstarterbe.login.model.ModalData;
import be.ordina.jworks.kickstarterbe.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @GetMapping(value = "/{username}", produces = {"application/json"})
    public ResponseEntity<ModalData> get(@PathVariable("username") String username) {
        ModalData data = this.loginService.login(username);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
