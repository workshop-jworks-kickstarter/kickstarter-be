FROM openjdk:8-jre

# Set the timezone.
RUN echo "Europe/Brussels" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

VOLUME /tmp
ADD target/kickstarter-be-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 8080
ENTRYPOINT ["java","-Xmx256m","-jar","/app.jar"]
